function [Area, Circumference] = circle(r)

Area = pi*r.^2;
Circumference = 2*pi*r;

end
