function [ H, D ] = projectile( v_initial, theta, time )
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here
global g
g =1

H = v_initial * time * sind(theta)-.5 * g * (time.^2);
D = v_initial * cosd(theta) * time;

end

