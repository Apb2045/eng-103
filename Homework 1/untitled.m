%%
%Task 1(a)
clc;
clear all;
a = 8*(10/17)+18/(3*7)+5*6^2
%%
%Task 1(b)
clc;
clear all;
b = 6*(31^(1/6))+12^(0.45)
%%
%Task 2
clc;
clear all;
x = -3 + 6i;
y = 5 - 2i;
a = x + y
b = x*y
c = x/y
%%
%Task 3
clc;
clear all;
a = cos(0):0.04:log10(200)
l = length(a)
e = a(30)
%%
%Task 4
clc;
clear all;
p=[1 -2 5 26];
sol = roots(p)
%%
%Task 5
clc;
clear all;
t = linspace(0,5,100);
s = 4*sin(4*t+4)+sqrt(8*t +15);
plot(t,s)
title('4*sin(4t+4)+sqrt(8*t +15)');
ylabel('s (speed in feet per second)');
xlabel('t (time in seconds)');

