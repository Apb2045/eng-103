%   Alex Babich
%   Homework 1


%%
%Task 1(a)
clc;
clear all;
a = 8*(10/17)+18/(3*7)+5*6^2
%%
%Task 1(b)
clc;
clear all;
b = 6*(31^(1/6))+12^(0.45)
%%
%Task 2
clc;
clear all;
x = -3 + 6i;
y = 5 - 2i;
a = x + y
b = x*y
c = x/y
%%
%Task 3
clc;
clear all;
a = cos(0):0.04:log10(200)
l = length(a)
e = a(30)
%%
%Task 4
clc;
clear all;
p=[1 -2 5 26];
sol = roots(p)
%%
%Task 5
clc;
clear all;
t = linspace(0,5,100);
s = 4*sin(4*t+4)+sqrt(8*t +15);
plot(t,s)
title('4*sin(4t+4)+sqrt(8*t +15)');
ylabel('s (speed in feet per second)');
xlabel('t (time in seconds)');
%%
%Task 6
clc;
clear all;
x = linspace(0,1.5,100);
y = 3*sqrt(5*x+1);
z = 5*exp(0.2*x)-4*x;
plot(x,y,'r',x,z,'b')
ylabel('Force (Newtons)');
xlabel('Distance (meters)');
title('y = 3*sqrt(5*x+1)   z = 5*e^(0.2*x)-4*x');
legend('y','z');
%%
%Task 7
clc;
clear all;
syms x y z
[x,y,z] = solve(x+y-3*z==10,x-y+2*z==3,2*x+y-z==-6)
%%
%Task 8
clc;
clear all;
r=p_input('Radius = ');
v = 4/3*pi*r^3;
x = ['The volume of the sphere is ', num2str(v)];
disp(x)

