%Homework 4
%Alex Babich
%Section S
%
%
%
%%
%Task 1
clc;
clear all;
x = linspace(10,50,3);
y = [4 5 6];
Left = log(x.*y)
Right = log(x)+log(y)
%%
%Task 2
clc;
clear all;
n=sqrt(4+5i);
Magnitude=abs(n)
Angle=angle(n)
Real=real(n)
Imag=imag(n)
%%
%Task 3
clc;
clear all;
x = linspace(10,50,3);
Left = exp(i*x)
Right = cos(x)+i*sin(x)
%%
%Task 4
clc;
clear all;
x = linspace(0,2*pi,3);
Left = asin(x) + acos(x)
Right = [pi/2 pi/2 pi/2]
%%
%Task 5
clc;
clear all;
x = linspace(pi/3,10/6*pi,3);
Left = tan(2*x)
Right= 2*tan(x)./(1-tan(x).^2)
%%
%Task 6
clc;
clear all;
x = linspace(1,5,3);
Left = sin(i*x)
Right= i*sinh(x)
%%
%Task 7
clc;
clear all;
type task7f;
x=linspace(-10,10,3);
task7f(x)
%%
%Task 8
clc;
clear all;
f=@(x) exp(-0.2*x)*sin(x+4)-0.4;
ezplot(f, [-5 5])
grid on

x1 = fzero(f,-3)
x2 = fzero(f, 1)
x3 = fzero(f, 3)
x4 = fzero(f, 4)
%%
%Task 9
clc;
clear all;
y=@(x) 5+exp(-0.4*x)*sin(x+4);
ezplot(y,[-5 10])
grid on

[x1,y1]=fminbnd(y,-4,1)
[x2,y2]=fminbnd(y,4,8)