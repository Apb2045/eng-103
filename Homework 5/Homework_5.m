%Homework 5
%Alex Babich
%Section S
%
%
%
%%
%Task 1
clc;
clear all;
type task1
Celsius=task1(80)
%%
%Task 2 part (a)
clc;
clear all;
syms A V r h
[h]=solve(V==(1/3)*pi*r^2*h,h)
[A]=solve(A==pi*r*sqrt(r^2+h^2),A)
%Task 2 part (c)
global V;
V=10;
ezplot(@task2,[0,10])
[r,A]=fminbnd(@task2,0,10)
eval(h)
type task2
%%
%Task 3
clc;
clear all;
f=@(x) 10*exp(-2*x);
ezplot(f, [0 2])
grid on
%%
%Task 4
clc;
clear all;
yzero=task4([-10,10])
type task4
%%
%Task 5
clc;
clear all;
f = task5(20,-200,12)
fminbnd(f,0,10)
type task5
%%
%Task 6
clc;
clear all;
load data.txt;
m=mean(data)